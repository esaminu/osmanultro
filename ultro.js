jobs = new Mongo.Collection('jobs')
itJobs = new Mongo.Collection('itJobs')
businessJobs = new Mongo.Collection('businessJobs')
bookmarks = new Mongo.Collection('bookmarks')
var edid;
if (Meteor.isClient) {

  Template.job.helpers({
    noOfJobs: function() {
      return jobs.find().count();
    },
    jobs: function() {
      return jobs.find();
    },
  });

  Template.bookmark.helpers({
    /*counter: function () {
    }*/
    noOfJobs: function() {
      return bookmarks.find().count();
    },
    bookmarks: function() {
      return bookmarks.find();
    }
  });

  Template.itJob.helpers({
    /*counter: function () {
    }*/
    noOfJobs: function() {
      return itJobs.find().count();
    },
    itJobs: function() {
      return itJobs.find();
    }
  });

  Template.businessJob.helpers({
    /*counter: function () {
    }*/
    noOfJobs: function() {
      return businessJobs.find().count();
    },
    businessJobs: function() {
      return businessJobs.find();
    }
  });

  Template.body.helpers({
    noOfJobs: function() {
      return jobs.find().count();
    }
  });

  Template.body.events({
    'click #it': function () {
      Router.go('/it');
    },
    'click #business': function () {
      Router.go('/business');
    },
    'click #all': function () {
      Router.go('/');
    },
    'click #bookmarks': function () {
      Router.go('/bookmarks');
    }
  });

  Template.job.events({
    'click #addBook': function() {
      var title = this.title
      var content = this.content
      bookmarks.insert({title: title, content: content});
    },
    'click .red': function() {
      var dialog = document.getElementById("dialog");
      dialog.open();
    },
    'click .colorful': function() {
      var title = document.getElementById("jlabel").value;
      var content = document.getElementById("dlabel").value;
      if(title!="" && content!=""){
        if(document.getElementById("ccheck").checked) {
          itJobs.insert({title: title, content: content});
        }
        if(document.getElementById("bcheck").checked) {
          businessJobs.insert({title: title, content: content});
        }
        jobs.insert({title: title, content: content});
        document.getElementById("dialog").close();
      }
    },
    'click #edit': function() {
      edid = this._id;
      var dialog = document.getElementById("dialog1");
      dialog.open();
      document.getElementById("jolabel").value = this.title;
      document.getElementById("delabel").value = this.content;
    },
    'click .update': function() {
      var title = document.getElementById("jolabel").value;
      var content = document.getElementById("delabel").value;
      jobs.insert({title: title, content: content});
      jobs.remove(edid);
      var dialog = document.getElementById("dialog1");
      dialog.close();
    },
    'click #remove': function() {
      jobs.remove(this._id);
    }
  });
  Template.itJob.events({
    'click #addBook': function() {
      var title = this.title
      var content = this.content
      bookmarks.insert({title: title, content: content});    
    },
    'click #edit': function() {
      edid = this._id;
      var dialog = document.getElementById("dialog1");
      dialog.open();
      document.getElementById("jolabel").value = this.title;
      document.getElementById("delabel").value = this.content;
    },
    'click .update': function() {
      var title = document.getElementById("jolabel").value;
      var content = document.getElementById("delabel").value;
      itJobs.insert({title: title, content: content});
      itJobs.remove(edid);
      var dialog = document.getElementById("dialog1");
      dialog.close();
    },
    'click #remove': function() {
      itJobs.remove(this._id);
    }
  });
  Template.businessJob.events({
    'click #addBook': function() {
      var title = this.title
      var content = this.content
      bookmarks.insert({title: title, content: content});
    },
    'click #edit': function() {
      edid = this._id;
      var dialog = document.getElementById("dialog1");
      dialog.open();
      document.getElementById("jolabel").value = this.title;
      document.getElementById("delabel").value = this.content;
    },
    'click .update': function() {
      var title = document.getElementById("jolabel").value;
      var content = document.getElementById("delabel").value;
      businessJobs.insert({title: title, content: content});
      businessJobs.remove(edid);
      var dialog = document.getElementById("dialog1");
      dialog.close();
    },
    'click #remove': function() {
      businessJobs.remove(this._id);
    }
  });
  Template.bookmark.events({
    'click #remBook': function() {
      bookmarks.remove(this._id);
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
