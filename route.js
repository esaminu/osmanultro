Router.configure({
    layoutTemplate: 'body'
});

Router.route('/', function () {
  this.render('job');
});
 
Router.route('/it', function () {
  this.render('itJob');
});

Router.route('/business', function () {
  this.render('businessJob');
});

Router.route('/bookmarks', function () {
  this.render('bookmark');
});